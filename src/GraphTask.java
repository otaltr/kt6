/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      int numberOfVertexes = 6;
      int numberOfArcs = 10;
      g.createRandomSimpleGraph (numberOfVertexes, numberOfArcs);
      System.out.println("Graph before deleting:");
      System.out.println (g);

      Vertex vertexToDelete = new Vertex("v5");
      System.out.println("Vertex " + vertexToDelete + " and its parents are now being deleted from the graph...");
      g.deleteVertexAndItsPredecessors(vertexToDelete);

      System.out.println();
      System.out.println("Graph after deleting:");
      System.out.println (g);

   }

   /**
    * Vertex is a point or a node to which Arcs may be connected to and from.
    */
   class Vertex {

      private String id;
      private Vertex nextVertex;
      private Arc firstArc;
      private int defaultInfo = 0;
      private int vertexInfo = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         nextVertex = v;
         firstArc = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      /** Set the info value for the Vertex.*/
      public void setVInfo(int i) {
         vertexInfo = i;
      }

      /** Return the id value of the Vertex.*/
      public String getVertexId() {
         return id;
      }

      /** Return the info value of the Vertex.*/
      public int getVertexInfo() {
         return vertexInfo;
      }
   }


   /** Arc represents one arrow in the graph between two Vertexes.*/
   class Arc {

      private String arcId;
      private Vertex targetVertex;
      private Arc nextArc;
      private int arcInfo = 0;

      Arc (String s, Vertex v, Arc a) {
         arcId = s;
         targetVertex = v;
         nextArc = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return arcId;
      }

      /** Set the info value for the Arc.*/
      public void setArcInfo(int i) {
         arcInfo = i;
      }

      /** Return the info value of the Arc.*/
      public int getArcInfo() {
         return arcInfo;
      }

      public void setNextArc(Arc a) {
         nextArc = a;
      }
   }

   /** Graph acts as a the main structure for this program.
    * It is made up from Vertexes and Arcs connecting them.
    * */
   class Graph {

      private String graphId;
      private Vertex firstVertex;
      private int graphInfo = 0;

      Graph (String s, Vertex v) {
         graphId = s;
         firstVertex = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /** Prints out a human-readable representation of the graph.
       * Each line represents a vertex and arcs exiting this vertex.*/
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (graphId);
         sb.append (nl);
         Vertex v = firstVertex;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.firstArc;
            while (a != null) {
                  sb.append (" ");
                  sb.append (a.toString());
                  sb.append (" (");
                  sb.append (v.toString());
                  sb.append ("->");
                  if (a.targetVertex != null) {
                     sb.append (a.targetVertex.toString());
                  }
                  sb.append (")");
               a = a.nextArc;
            }
            sb.append (nl);
            v = v.nextVertex;
         }
         return sb.toString();
      }

      /**
       * Create a new Vertex in the graph.
       * @param vid name of the new Vertex
       * @return new Vertex created
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.nextVertex = firstVertex;
         firstVertex = res;
         return res;
      }


      /**
       * Create a new Arc in the graph.
       * @param aid ame of the new Arc
       * @param from Vertex that the Arc exits
       * @param to Vertex which Arc points to
       * @return new Arc created
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.nextArc = from.firstArc;
         from.firstArc = res;
         res.targetVertex = to;
         return res;
      }


      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("A=" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         graphInfo = 0;
         Vertex v = firstVertex;
         while (v != null) {
            v.defaultInfo = graphInfo++;
            v = v.nextVertex;
         }
         int[][] res = new int [graphInfo][graphInfo];
         v = firstVertex;
         while (v != null) {
            int i = v.defaultInfo;
            Arc a = v.firstArc;
            while (a != null) {
               int j = a.targetVertex.defaultInfo;
               res [i][j]++;
               a = a.nextArc;
            }
            v = v.nextVertex;
         }
         return res;
      }

      /**
       * Create a connected simple (directed, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of arcs: " + m);
         firstVertex = null;
         createRandomTree (n);       // n-1 arcs created here
         Vertex[] vert = new Vertex [n];
         Vertex v = firstVertex;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.nextVertex;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining arcs
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple arcs
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("A=" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            edgeCount--;  // a new arc happily created
         }
      }


      /**
       * Delete a Vertex and its predecessors (parents) from the Graph.
       * Two conditions must be met before deleting:
       * 1) input graph ('this') must be valid,
       * 2) vertex must exist in the graph ('this')
       * Otherwise an exception is thrown to the user.
       *
       * @param vertexToDelete Vertex to be 'deleted' from the graph
       */

      public void deleteVertexAndItsPredecessors(Vertex vertexToDelete) {

         if (firstVertex == null) {
            throw new IllegalArgumentException("Can't delete a vertex from an empty graph!");
         }

         try {
            markGraphElementsToBeDeleted(vertexToDelete);
         } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Vertex requested to be deleted does not exist in the graph!");
         }

         // SET NEW firstVertex, IF NECESSARY
         setNewFirstVertex();

         // DELETE PREDECESSOR VERTEXES
         deletePredecessors();

         // DELETE INVALID ARCS FOR EACH LEFTOVER VERTEX
         deleteInvalidArcs();
      }

      /**
       * Method marks all the Vertexes and Arcs related to the parameter vertexToDelete.
       * Info values are set according to the following scheme:
       * 1 -- Vertex requested to be deleted by the user
       * 2 -- Arc pointing to the Vertex requested to be deleted
       * 3 -- Parent/predecessor Vertex for the Vertex requested to be deleted
       * 4 -- Arc pointing to the parent/predecessor Vertex
       *
       * Method also checks if the Vertex requested to be deleted even exists in the graph and throws an
       * exception, if not found.
       *
       * @param vertexToDelete vertex to be deleted from the graph
       */
      private void markGraphElementsToBeDeleted(Vertex vertexToDelete) {
         boolean found = false;
         Vertex vertex1 = firstVertex;
         while (vertex1 != null) {
            if (vertex1.getVertexId().equals(vertexToDelete.getVertexId())) {
               vertex1.setVInfo(1);
               found = true;
            }
            Arc arc = vertex1.firstArc;
            while (arc != null) {

               if (arc.targetVertex.getVertexId().equals(vertexToDelete.getVertexId())) {
                  arc.targetVertex.setVInfo(1);
                  arc.setArcInfo(2);
               }
               if (arc.getArcInfo() == 2 && !vertex1.getVertexId().equals(vertexToDelete.getVertexId())) {
                  vertex1.setVInfo(3);
               }

               arc = arc.nextArc;
            }
            vertex1 = vertex1.nextVertex;
         }
         if (!found) {
            throw new IllegalArgumentException();
         }

         Vertex vertex2 = firstVertex;
         while (vertex2 != null) {
            Arc a = vertex2.firstArc;
            while (a != null) {
               if (a.targetVertex.getVertexInfo() == 3) {
                  a.setArcInfo(4);
               }
               a = a.nextArc;
            }
            vertex2 = vertex2.nextVertex;
         }
      }

      /**
       * Method sets a new firstVertex (an entry point for the graph) if the current firstVertex was marked as
       * a Vertex to be deleted (info value 1 or 3).
       * If a new firstVertex is required but was impossible to assign, method returns and the graph will eventually
       * become empty.
       */
      public void setNewFirstVertex() {
         int counter = 0; // COUNTS HOW MANY VERTEXES HAVE BEEN INSPECTED
         Vertex vertex = firstVertex;
         while (vertex != null) {
            if (vertex.getVertexInfo() == 3 && counter == 0 || vertex.getVertexInfo() == 1 && counter == 0) { // IF FIRST VERTEX NEEDS TO BE DELETED, SET A NEW firstVertex
               if (vertex.nextVertex != null) {
                  Vertex currentVertex = vertex.nextVertex;
                  if (currentVertex.getVertexInfo() == 1 || currentVertex.getVertexInfo() == 3) {
                     while (currentVertex.getVertexInfo() == 1 || currentVertex.getVertexInfo() == 3) { // LOOK FOR THE NEXT FIRST VERTEX THAT DOES NOT REQUIRE TO BE DELETED
                        if (currentVertex.nextVertex != null) {
                           currentVertex = currentVertex.nextVertex;
                        } else { // firstVertex CAN'T BE SET TO NO OTHER VERTEX
                           firstVertex = null; // DELETE CURRENT VERTEX
                           break;
                        }
                     }
                     if (firstVertex == null) {  // GRAPH WILL BECOME EMPTY
                        return;
                     }
                  }
                  firstVertex = currentVertex; // SET NEW firstVertex

               } else { // firstVertex CAN'T BE SET TO NO OTHER VERTEX -> GRAPH WILL BECOME EMPTY
                  firstVertex = null;
                  return;
               }
            }
            counter++;
            vertex = vertex.nextVertex;
         }
      }

      /**
       * Deletes all predecessors / parents of the vertex to be deleted by the user.
       * New 'nextVertex' values are set to delete / "forget" parent vertexes.
       * By doing that, all arcs exiting these parent vertexes (marked with info value '2') are also "forgotten".
       */
      public void deletePredecessors() {
         Vertex vertex = firstVertex;
         while (vertex != null) {
            if (vertex.nextVertex != null) {
               if (vertex.nextVertex.getVertexInfo() == 1 || vertex.nextVertex.getVertexInfo() == 3) { // IF nextVertex NEEDS TO BE DELETED
                  if (vertex.nextVertex.nextVertex != null) {
                     Vertex currentVertex = vertex.nextVertex.nextVertex;
                     if (currentVertex.getVertexInfo() == 1 || currentVertex.getVertexInfo() == 3) {
                        while (currentVertex.getVertexInfo() == 1 || currentVertex.getVertexInfo() == 3) { // LOOK FOR THE NEXT VERTEX THAT DOES NOT REQUIRE TO BE DELETED
                           if (currentVertex.nextVertex != null) {
                              currentVertex = currentVertex.nextVertex;
                           } else {                      // nextVertex CAN'T BE SET TO NO OTHER VERTEX
                              vertex.nextVertex = null;
                              break;
                           }
                        }
                     }
                     if (vertex.nextVertex != null) {
                        vertex.nextVertex = currentVertex; // SET NEW nextVertex
                     }
                  } else {
                     vertex.nextVertex = null;  // nextVertex CAN'T BE SET TO NO OTHER VERTEX
                  }
               }

            }
            vertex = vertex.nextVertex;
         }
      }

      /**
       * Deletes all arcs pointing to the parent/predecessor Vertexes that were just deleted (and now appear as 'null').
       * These arcs were previously marked with info value '4'.
       */
      public void deleteInvalidArcs() {
         Vertex vertex = firstVertex;
         while (vertex != null) {
            boolean arrayIsRequired = false;
            int arraySize = 0;
            Arc a1 = vertex.firstArc;
            while (a1 != null) {       // COUNT HOW MANY ARCS EXIT CURRENT VERTEX
               arraySize++;
               a1 = a1.nextArc;
               arrayIsRequired = true;
            }
            if (arrayIsRequired) {        // IF NO ARCS EXIT VERTEX THEN THE FOLLOWING IS NOT NEEDED

               Arc[] arcs = getArcArray(vertex, arraySize);

               markArcsToBeDeleted(arcs);

               setNewArcSequence(vertex, arcs);
            }

            vertex = vertex.nextVertex; // REPEAT FOR ALL VERTEXES
         }
      }

      /**
       * Gets all arcs from a vertex and collects them into an array.
       *
       * @param vertex the Vertex that all exiting Arcs are collected from
       * @param arraySize number of Arcs exiting this Vertex
       * @return array of arcs
       */
      public Arc[] getArcArray(Vertex vertex, int arraySize) {
         Arc[] arcs = new Arc[arraySize];
         int index = 0;
         Arc arc = vertex.firstArc;
         while (arc != null) {
            arcs[index] = arc;
            index++;
            arc = arc.nextArc;
         }
         return arcs;
      }

      /**
       * Marks all arcs in the array as 'null' that are required to be deleted (marked with info value '4').
       *
       * @param arcs array of arcs
       */
      public void markArcsToBeDeleted(Arc[] arcs) {
         for (int j = 0; j < arcs.length; j++) {
            if (arcs[j].getArcInfo() == 4) {
               arcs[j] = null;
            }
         }
      }

      /**
       * Resets the arcs exiting the Vertex and assigns new arcs.
       * New 'firstArc' and new 'nextArcs' are set for the Vertex.
       *
       * @param vertex the Vertex to which a new sequence of arcs is assigned
       * @param arcs  array of arcs
       */
      public void setNewArcSequence(Vertex vertex, Arc[] arcs) {

         vertex.firstArc = null; // RESET FIRST ARC FOR CURRENT VERTEX

         if (arcs.length == 1) {
            if (arcs[0] != null) {
               vertex.firstArc = arcs[0];
            }
         }

         Arc currentArc = null;
         if (arcs.length > 1) {
            for (Arc arc : arcs) {
               if (arc != null) {
                  if (vertex.firstArc == null) { // IF NEW firstArc HAS NOT BEEN SET YET
                     vertex.firstArc = arc;
                     currentArc = vertex.firstArc;
                  } else {
                     currentArc.setNextArc(arc); // SET nextArc
                     currentArc = currentArc.nextArc;
                  }
                  currentArc.nextArc = null; // RESET nextArc
               }
            }
         }
      }
   }
} 

